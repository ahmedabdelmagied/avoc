import 'package:flutter/material.dart';
import 'Product.dart';

abstract class StoreData{
  List<Product> getFruits(){return new List<Product>();}
  List<Product> getVegatables(){return new List<Product>();}
  List<Product> getDrinks(){return new List<Product>();}
}

//get dummy data
class getStoreDummyData extends StoreData{
  List<Product> getFruits(){
      List<Product> fruits = new List<Product>();
      fruits.add(new Product("Tomatoes", 150.0, "assets/images/anans.png", 2.0));
      fruits.add(new Product("Tomatoes", 150.0,
          "assets/images/orange.png", 2.0));
      fruits.add(new Product("Tomatoes", 150.0,
          "assets/images/panana.png", 2.0));

      return fruits;
  }
  List<Product> getVegatables(){
    List<Product> Vegatables = new List<Product>();
    Vegatables.add(new Product("Tomatoes", 150.0,
        "assets/images/Tomates.png", 2.0));
    Vegatables.add( new Product("Tomatoes", 150.0,
        "assets/images/Tomates.png", 2.0));
    Vegatables.add( new Product("Tomatoes", 150.0,
        "assets/images/Tomates.png", 2.0));

    return Vegatables;
  }
  List<Product> getDrinks(){
    List<Product> Drinks = new List<Product>();
    Drinks.add(new Product("Tomatoes", 150.0, "assets/images/anans.png", 2.0));
    Drinks.add(new Product("Tomatoes", 150.0,
        "assets/images/orange.png", 2.0));
    Drinks.add(new Product("Tomatoes", 150.0,
        "assets/images/panana.png", 2.0));

    return Drinks;
  }
}





//TODO step1 GET DATA FROM FIREBASE
class getStoreFromFirebase extends StoreData{
  List<Product> getFruits(){
    List<Product> fruits = new List<Product>();
   //TODO fill the fruits list from firebase
    return fruits;
  }
  List<Product> getVegatables(){
    List<Product> Vegatables = new List<Product>();
    //TODO fill the fruits list from Vegatables
    return Vegatables;
  }
  List<Product> getDrinks(){
    List<Product> Drinks = new List<Product>();
    //TODO fill the fruits list from Drinks
    return Drinks;
  }
}