//import 'package:flutter/material.dart';
//
//
//class Product{
//  String productName;
//  double productPrice;
//  double productQuantity;
//
//  // temporary
//  String ImagePath;
//  Product(){};
//  factory Product.fromJson(Map<String, dynamic> json) {
//    return Product(
//      productName: json['userId'] as String,
//      productPrice: json['id'] as double,
//      productQuantity: json['title'] as double,
//      ImagePath: json['body'] as String,
//    );
//  }
//
//
//  Product(this._productName, this._productPrice, this._ImagePath , this._productQuantity);
//
//  double get productQuantity => _productQuantity;
//
//  set productQuantity(double value) {
//    _productQuantity = value;
//  }
//
//  String get productName => _productName;
//
//  set productName(String value) {
//    _productName = value;
//  }
//
//  double get productPrice => _productPrice;
//
//  set productPrice(double value) {
//    _productPrice = value;
//  }
//
//  String get ImagePath => _ImagePath;
//
//  set ImagePath(String value) {
//    _ImagePath = value;
//  }


//}/

import 'package:flutter/foundation.dart';

class Product {
   String _productName;
   double _productQuantity;
   double _productPrice;
   String _ImagePath;

   Product(this._productName, this._productQuantity, this._ImagePath ,this._productPrice);

   String get ImagePath => _ImagePath;

   set ImagePath(String value) {
     _ImagePath = value;
   }

   double get productPrice => _productPrice;

   set productPrice(double value) {
     _productPrice = value;
   }

   double get productQuantity => _productQuantity;

   set productQuantity(double value) {
     _productQuantity = value;
   }

   String get productName => _productName;

   set productName(String value) {
     _productName = value;
   }


}
