import 'package:flutter/material.dart';
import 'Product.dart';

abstract class ClientBasket{
  List<Product> getClientBaskets(){return new List<Product>();}
}


//get dummy data
class getClientBasketDummyData extends ClientBasket{
  List<Product> getClientBaskets(){

    List<Product> clientBaskets = new List<Product>();
    clientBaskets.add(new Product(
        "avokado", 120.0, "assets/images/avokado.png", 1.0));
    clientBaskets.add(new Product(
        "Tomatoes", 30.0, "assets/images/Tomates.png", 22.0));
    clientBaskets.add(new Product(
        "orange", 45.0, "assets/images/orange.png", 2.0));
    clientBaskets.add(new Product(
        "panana", 50.0, "assets/images/panana.png", 5.0));
    clientBaskets.add(new Product(
        "jhear", 12.0, "assets/images/jhear.png", 5.0));
    clientBaskets.add(new Product(
        "panana", 35.0, "assets/images/panana.png", 4.0));

    return clientBaskets;
  }
}



//TODO GET DATA FROM FIREBASE
class getClientBasketFromFirebase extends ClientBasket{
  List<Product> getClientBaskets(){

  }
}