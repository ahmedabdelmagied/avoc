class Strings {
  Strings._();

 // registration Screen Constant Strings.
  static const String phoneNumber = 'Phone Number';
  static const String registrationCardHeader = 'Phone Number';
  static const String registrationButton = 'Send Registration Code';
  static const String registrationMessage = 'The code will be sent in a text message';

}
