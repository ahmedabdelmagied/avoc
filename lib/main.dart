import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:how_connect_to_server/screens/home_page/home_page.dart';
import 'screens/product/product_details.dart';
import 'model/Product.dart';
const bool debugEnableDeviceSimulator = false;

void main() {

  runApp(new MaterialApp(
    title: 'DeviceSimulator demo',
    debugShowCheckedModeBanner: false,
    home: DeviceSimulator(
      brightness: Brightness.dark,
      enable: debugEnableDeviceSimulator,
//      child: new MyApp()
    child:new MyApp(),
    ),
  )
//      new MaterialApp(
//    home: new MyApp(),
//    debugShowCheckedModeBanner: false,
//  )
      );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(body: HomePage());
  }
}
