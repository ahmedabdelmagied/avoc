import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class SideMenu extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        'assets/images/side_menu_image/side_menu_top.png'),
                    fit: BoxFit.fill)),
          ),
          ListTile(
//             onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (context)=>myBasket())),
              onTap: () {},
              leading: Image.asset('assets/images/side_menu_image/Sala.png',),
              title: Text(
                "My Basket",
                style: TextStyle(

                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
          ListTile(
//          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>pliticsAndSafety())),
              leading: Image.asset('assets/images/side_menu_image/policy.png'),
              onTap: () {},
              title: Text(
                "Politics and Safety",
                style: TextStyle(
                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
          ListTile(
//          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>contactUs())),
              onTap: () {},
              leading: Image.asset('assets/images/side_menu_image/contact.png'),
              title: Text(
                "Contact US",
                style: TextStyle(
                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
          ListTile(
//              onTap: () => Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => aboutCompany())),
              leading: Image.asset('assets/images/side_menu_image/about.png'),
              title: Text(
                "About Company",
                style: TextStyle(

                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
          ListTile(
              leading: Image.asset('assets/images/side_menu_image/language.png'),
              title: Text(
                "Change Language",
                style: TextStyle(

                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
          ListTile(

//              onTap: () => Navigator.push(
//                  context, MaterialPageRoute(builder: (context) => signup())),
              leading: Image.asset('assets/images/side_menu_image/login.png'),
              title: Text(
                "Login",
                style: TextStyle(

                  fontFamily: 'SourceSansPro',
                  fontSize: MediaQuery.of(context).size.height >= 736?20: 14,
                ),
              )),
        ],
      ),
    );
  }
}
