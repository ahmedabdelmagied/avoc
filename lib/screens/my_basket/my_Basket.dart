import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

import '../../model/Product.dart';
import '../../model/clientBasket.dart';
import 'widgets/productBasket_card.dart';

class myBasket extends StatefulWidget {
  ClientBasket clientBasket;
  myBasket({this.clientBasket});
  _myBasket createState() => new _myBasket();
}

class _myBasket extends State<myBasket> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints)
    {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return ListView(
          children: <Widget>[
            Container(
              width: SizeConfig.getResponsiveWidth(60.0),
              height: SizeConfig.getResponsiveHeight(70.0),
              child: Center(
                child: Image.asset(
                  "assets/images/sala.png",
                  color: Colors.green,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: SizeConfig.getResponsiveHeight(50.0)),
            child: GridView.count(
              mainAxisSpacing: SizeConfig.getResponsiveHeight(0),
                shrinkWrap: true,
                physics: ScrollPhysics(),
                childAspectRatio: 1.0,
                crossAxisSpacing: SizeConfig.getResponsiveWidth(0.0),
                crossAxisCount: MediaQuery.of(context).size.width >= 768 ? 3: 2,
              children: List.generate(widget.clientBasket.getClientBaskets().length, (index){
                return  makeProductCard(widget.clientBasket.getClientBaskets().elementAt(index));
              }),
            ),
            )
          ],
        );
      });
    });
  }
}

Widget makeProductCard(Product productItem) {
  return Container(
    height: SizeConfig.getResponsiveHeight(70.0),
    width: SizeConfig.getResponsiveWidth(80.0),
    child: FittedBox(
        fit: BoxFit.contain, child: productCard(product: productItem)),
  );
}
