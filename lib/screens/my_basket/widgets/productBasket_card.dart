import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

import '../../../model/Product.dart';
import '../../product/product_details.dart';

class productCard extends StatefulWidget {
  Product product;

  int quantity = 5;

  productCard({this.product, this.quantity});

  @override
  _productCardState createState() => _productCardState();
}

class _productCardState extends State<productCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: SizeConfig.getResponsiveWidth(320.0),
        height:SizeConfig.getResponsiveHeight(280.0),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          color: Colors.white,
          elevation: 3,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(SizeConfig.getResponsiveWidth(20.0)),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(5.0))),
                    width: SizeConfig.getResponsiveWidth(90.0),
                    height: SizeConfig.getResponsiveHeight(25.0),
                    child: Center(
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Text(
                          "${widget.product.productQuantity.toString()} kg",
                          style: new TextStyle(
                              color: Colors.white70,
                              fontWeight: FontWeight.bold,
                              fontSize: SizeConfig.getResponsiveWidth(25.0)),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(15.0),
                    child: new Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: SizeConfig.getResponsiveWidth(45.0),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: SizeConfig.getResponsiveWidth(12.0), bottom:SizeConfig.getResponsiveHeight(5.0)),
                        child: Text(
                          "${widget.product.productName}",
                          maxLines: 3,
                          //   overflow:TextOverflow. ,
                          style: TextStyle(
                            fontSize: SizeConfig.getResponsiveWidth(20.0),
//                        fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) =>
                              new productDetails(widget.product, widget.quantity)));
                        },
                        child: Container(
                          height: SizeConfig.getResponsiveHeight(120.0),
                          width: SizeConfig.getResponsiveWidth(200.0),
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: Image.asset(widget.product.ImagePath),
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(right: SizeConfig.getResponsiveWidth(80.0)),
                        child: Text(
                          "LE ${widget.product.productPrice.toString()}",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontSize: SizeConfig.getResponsiveWidth(20),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(right: SizeConfig.getResponsiveWidth(20.0), top: SizeConfig.getResponsiveHeight(20.0)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: SizeConfig.getResponsiveHeight(35.0),
                          width: SizeConfig.getResponsiveWidth(30.0),
                          child: Center(
                              child: Text(
                            "+",
                            style: TextStyle(fontSize: SizeConfig.getResponsiveWidth(25.0)),
                          )),
                          decoration:
                              BoxDecoration(border: Border.all(width: SizeConfig.getResponsiveWidth(2))),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: SizeConfig.getResponsiveHeight(15.0), bottom: SizeConfig.getResponsiveHeight(15.0)),
                          child: Text(
                            "1",
                            style: TextStyle(fontSize: SizeConfig.getResponsiveWidth(25.0)),
                          ),
                        ),
                        Container(
                          height: SizeConfig.getResponsiveHeight(35.0),
                          width: SizeConfig.getResponsiveWidth(30.0),
                          child: Center(
                              child: Text(
                            "-",
                            style: TextStyle(fontSize:SizeConfig.getResponsiveWidth(25.0)),
                          )),
                          decoration:
                              BoxDecoration(border: Border.all(width: SizeConfig.getResponsiveWidth(2))),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
