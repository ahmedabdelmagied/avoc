import 'package:flutter/material.dart';
import '../home_page/appbar/appbar_icon.dart';

AppBar myCartAppbar(GlobalKey<ScaffoldState> _scaffoldKey , Widget titleWidget) {
  return AppBar(
    centerTitle: true,
    title: titleWidget,
    elevation: 0.0,
    leading: Container(
        child: InkWell(
      onTap: () => _scaffoldKey.currentState.openDrawer(),
      child: Icon(
        Icons.format_align_left,
        color: Colors.white,
      ),
    )),
    iconTheme: IconThemeData(color: Colors.black),
    //add this line her
    backgroundColor: Color(0XFF21d493),
    actions: <Widget>[
      AppbarIcon(iconPath: 'assets/images/icons/bell.png',top: 10.0 ,right: 9.0,notificationColor: Colors.yellowAccent,number: 2,),
      AppbarIcon(iconPath: 'assets/images/icons/sala_top.png',top: 10.0 ,left: 9.0,notificationColor: Colors.red,number:5,),
    ],
  );
}
