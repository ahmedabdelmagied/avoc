import 'package:flutter/material.dart';

import '../../../model/Product.dart';

class productCard extends StatefulWidget {
  Product product;
  bool detailedProduct = true;

  productCard({this.product , this.detailedProduct});

  @override
  _productCardState createState() => _productCardState();
}

class _productCardState extends State<productCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
//      margin: EdgeInsets.all(25.0),
        width: 250.0,
        height: 170.0,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          color: Colors.white,
          elevation: 15,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(5.0))),
                    width: 80.0,
                    height: 25.0,
                    child: Center(
                      child:FittedBox(fit: BoxFit.contain,child: Text(
                        "${widget.product.productQuantity.toString()} kg",
                        style: new TextStyle(
                            color: Colors.white70,
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0),
                      ),),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(15.0),
                    child: new Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: 30.0,
                    ),
                  )
                ],
              ),

              Container(
                height: 85.0,
                width: 150.0,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset(widget.product.ImagePath),
                ),
              ),
            ],
          ),
        ));
  }
}
