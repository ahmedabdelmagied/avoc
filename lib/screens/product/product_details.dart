import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:how_connect_to_server/utils/size_config.dart';
import 'widgets/button_widget.dart';
import '../../model/Product.dart';
import 'widgets/product_card.dart';

import '../../model/Product.dart';

class productDetails extends StatelessWidget {
  Product currentProduct;
  int quantity;
  productDetails(this.currentProduct , this.quantity);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints)
    {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Color(0XFF21d493),
              leading: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  margin: EdgeInsets.only(top: SizeConfig.getResponsiveHeight(10.0)), child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                  size:SizeConfig.getResponsiveHeight(20),
                ),),),
              title: Container(margin: EdgeInsets.only(top: 10.0),
                  child: Text("Deal with Category",
                      style: TextStyle(fontSize: SizeConfig.getResponsiveHeight(15.0), color: Colors.white))),
              elevation: 0.0,
            ),
            body: SafeArea(
                child: Container(
                    color: Color(0XFF21d493),
                    child: Center(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: SizeConfig.getResponsiveHeight(60.0),bottom: SizeConfig.getResponsiveHeight(5.0)),
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                elevation: 20.0,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  width:(320.0),
                                  height: (450),
                                  child: ListView(
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(top: (120.0)),
                                            child: Center(
                                              child: Text(
                                                "${currentProduct.productName}",
                                                style: TextStyle(
                                                    fontSize: (23.0)),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: (4.0)),
                                            child: Center(
                                              child: Text(
                                                "LE ${currentProduct
                                                    .productPrice.toString()}",
                                                style: TextStyle(
                                                    fontSize: (15.0)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Center(
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: (10.0), bottom: (20.0)),
                                            child: RatingBar(
                                              initialRating: 3,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: (25.0),
                                              itemPadding:
                                              EdgeInsets.symmetric(
                                                  horizontal: 0.0),
                                              itemBuilder: (context, _) =>
                                                  Icon(
                                                    Icons.star,
                                                    color: Colors.amber,
                                                  ),
                                            ),
                                          )),


                                      Container(
                                        margin: EdgeInsets.only(bottom: (45.0)),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 30.0, bottom: 15.0),
                                              child: Text("Quantity",
                                                style: TextStyle(
                                                    fontSize: 19.0),),
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment
                                                  .center,
                                              children: <Widget>[
                                                Container(
                                                  height: 25.0,
                                                  width: 25.0,
                                                  child: Center(
                                                      child: Text(
                                                        "+",
                                                        style: TextStyle(
                                                            fontSize: 20.0),
                                                      )),
                                                  decoration:
                                                  BoxDecoration(
                                                      border: Border.all(
                                                          width: 2)),
                                                ),
                                                Container(
                                                  width: 20.0,
                                                  margin: EdgeInsets.only(
                                                      right: 10.0, left: 10.0),
                                                  child: Center(child: Text("5",
                                                    style: TextStyle(
                                                        fontSize: 20.0),)),
                                                ),
                                                Container(
                                                  height: 25.0,
                                                  width: 25.0,
                                                  child: Center(
                                                      child: Text(
                                                        "-",
                                                        style: TextStyle(
                                                            fontSize: 25.0),
                                                      )),
                                                  decoration:
                                                  BoxDecoration(
                                                      border: Border.all(
                                                          width: 2)),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),),
                                      registrationButton()
                                    ],
                                  ),
                                )),
                          ),
                          Container(margin: EdgeInsets.only(left: 60.0),
                              width: 220.0,
                              height: 200.0,
                              child: FittedBox(fit: BoxFit.contain,
                                  child: productCard(product: new Product(
                                      currentProduct.productName,
                                      currentProduct.productQuantity,
                                      currentProduct.ImagePath,
                                      currentProduct.productPrice),)))
                        ],
                      ),
                    ))));
      });
    });
  }
}
