import 'package:flutter/material.dart';

import '../../../constants/strings.dart';
import '../../../utils/size_config.dart';

class registrationButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 2.73 * SizeConfig.heightMultiplier),
      child: new FlatButton(
        padding: EdgeInsets.all(7.0),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0)),
        color: Color(0XFF21d493),
        child: new Text(
          Strings.registrationButton,
          style: TextStyle(
              fontSize: 2.1 * SizeConfig.textMultiplier, color: Colors.white),
        ),
        onPressed: () {},
      ),
    );
  }
}
