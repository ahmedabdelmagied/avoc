import 'package:flutter/material.dart';

import '../../../constants/images.dart';
import '../../../constants/strings.dart';
import '../../../utils/size_config.dart';
import 'button_widget.dart';
import 'textField_widget.dart';

class registrationCardLandScapeWidget extends StatefulWidget {
  @override
  _registrationCardLandScapeWidgetState createState() =>
      _registrationCardLandScapeWidgetState();
}

class _registrationCardLandScapeWidgetState
    extends State<registrationCardLandScapeWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
            left: 9.73 * SizeConfig.widthMultiplier,
            right: 9.73 * SizeConfig.widthMultiplier,
            top: 13.67 * SizeConfig.heightMultiplier,
            bottom: 6.84 * SizeConfig.heightMultiplier),
        alignment: Alignment.center,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 10.0,
          child: Container(
            height: 40.4 * SizeConfig.heightMultiplier,
            width: 150.15 * SizeConfig.widthMultiplier,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: new Container(
                    margin: EdgeInsets.only(top: 20.0),
                    child: Text(
                      Strings.registrationCardHeader,
                      style: TextStyle(
                          fontSize: (24.0 / 4.11) * SizeConfig.widthMultiplier,
                          color: Colors.black87,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        registrationTextField(),
                        registrationButton(),
                        Container(
                          margin: EdgeInsets.only(
                              top: 10 * SizeConfig.heightMultiplier,
                              bottom: 0.2 * SizeConfig.heightMultiplier),
                          child: Text(
                            Strings.registrationMessage,
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 2.0 * SizeConfig.textMultiplier),
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: 60.99 * SizeConfig.widthMultiplier,
                        height: 32 * SizeConfig.heightMultiplier,
                        child: Image.asset(Images.Sign_up),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
