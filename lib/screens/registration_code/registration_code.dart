import 'package:flutter/material.dart';

import '../../utils/size_config.dart';
import 'registration_widgets/cardLandscape_widget.dart';
import 'registration_widgets/card_widget.dart';
import 'registration_widgets/logo_widget.dart';

class signup extends StatelessWidget {
  Widget cardWidget = registrationCardWidget();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(
        builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          cardWidget = (SizeConfig.isPortrait)
              ? registrationCardWidget()
              : registrationCardLandScapeWidget();
          return new Scaffold(
              body: SingleChildScrollView(
            child: new SafeArea(
                child: new Stack(
              children: <Widget>[
                registrationLogo(),
                Positioned(
                  top: 10,
                  left: 5,
                  child: Icon(
                    Icons.arrow_back_ios,
                    size: 6.8 * SizeConfig.heightMultiplier,
                    color: Colors.white,
                  ),
                ),
                cardWidget
              ],
            )),
          ));
        },
      );
    });
  }
}
