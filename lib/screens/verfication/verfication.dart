import 'package:flutter/material.dart';
import 'button_widget.dart';

class Verification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Color(0XFF21d493),
      child: new Center(
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 20.0,
            child: Container(
              width: 320.0,
              height: 600,
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 40.0),
                    child: Center(
                      child: Text(
                        "Verification",
                        style: TextStyle(fontSize: 30.0),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10.0),
                      width: 120.0,
                      height: 220.0,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset("assets/images/verfication.png"),
                      )),
                  Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 5.0, bottom: 9.0),
                          child: Text(
                            "we have sent the verfication",
                            style: TextStyle(fontSize: 20.0),
                          ),
                        ),
                        Center(
                          child: Text(
                            "code to your phone",
                            style: TextStyle(fontSize: 20.0),
                          ),
                        )
                      ],
                    ),
                  ),
                 Container(
                    width: 200.0,
                    height: 40.0,
                     color: Color(0xffd7f7ea),
                    margin: EdgeInsets.only(bottom: 70.0,top: 30.0,right: 35.0 , left: 35.0),
                    child: TextField(
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.blueAccent,
                        ),
                        decoration: InputDecoration(
                          hintStyle: TextStyle(fontSize: 18.0),
                        prefixText:"mm ",
                          hintText: "Enter Verification Code",
                        )),
                  ),

                   registrationButton()


                ],
              ),
            )),
      ),
    ));
  }
}
