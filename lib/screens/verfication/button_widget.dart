import 'package:flutter/material.dart';

import '../../constants/strings.dart';
import '../../utils/size_config.dart';

class registrationButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        right: 15.0,
        left: 15.0
      ),
      child: new FlatButton(
        padding: EdgeInsets.all(7.0),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0)),
        color: Color(0XFF21d493),
        child: new Text(
          "Submit"  ,
          style: TextStyle(
              fontSize: 20, color: Colors.white),
        ),
        onPressed: () {},
      ),
    );
  }
}
