import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';
import 'widgets/profile_header.dart';
import 'widgets/profile_body.dart';


class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints)
    {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom:SizeConfig.getResponsiveHeight(2.0) ),
                height:  SizeConfig.getResponsiveHeight(150.0),
                decoration: BoxDecoration(
                  color: Color(0XFF21d493),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(SizeConfig.getResponsiveHeight(20)),
                      bottomLeft: Radius.circular(SizeConfig.getResponsiveHeight(20.0))),
                ),

                child: Center(
                    child: ProfileHeader()
                )),

            ProfileBody()
          ],
        );
      });
    });
  }
}
