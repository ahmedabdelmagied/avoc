import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class ProfileHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage("assets/images/profile.png"),
           radius: SizeConfig.getResponsiveHeight(40),
          backgroundColor: Colors.transparent,
        ),
        Container(
          child: Text(
            "Ebrahim Mohamed",
            style: TextStyle(fontSize:  SizeConfig.getResponsiveHeight(15.0), color: Colors.white),
          ),
        ),
        Container(
          child: Text(
            "ebrahim_mohamed@gmail.com",
            style: TextStyle(fontSize: SizeConfig.getResponsiveHeight(12.0), color: Colors.white),
          ),
        ),
        Container(
          child: Text(
            "01144901863",
            style: TextStyle(fontSize: SizeConfig.getResponsiveHeight(12.0), color: Colors.white),
          ),
        )
      ],
    );
  }
}
