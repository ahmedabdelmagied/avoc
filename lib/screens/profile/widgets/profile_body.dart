import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class ProfileBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: <Widget>[
      profileItem("My Basket", "assets/images/side_menu_image/Sala.png",context),
      profileItem(
          "Politics and Safety", 'assets/images/side_menu_image/policy.png',context),
      profileItem("Contact US", 'assets/images/side_menu_image/contact.png',context),
      profileItem("About Company", 'assets/images/side_menu_image/about.png',context),
      profileItem(
              "Change Language", 'assets/images/side_menu_image/language.png',context),
          profileItem("Login", 'assets/images/side_menu_image/login.png',context),

    ]);
  }
}

Widget profileItem(String itemName, String imagePath,BuildContext context) {
  return Container(
    margin: MediaQuery.of(context).size.height >= 720? EdgeInsets.only(bottom: 15):EdgeInsets.only(bottom: 0),
    child: ListTile(
        dense: true,
//              onTap: () => Navigator.push(
//                  context, MaterialPageRoute(builder: (context) => signup())),
        leading: Image.asset(imagePath , color: Colors.grey,),
        title: Text(
          "${itemName}",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: 'SourceSansPro',
            fontSize: SizeConfig.getResponsiveHeight(13),
            color: Colors.grey
          ),
        )),
  );
}
