import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:how_connect_to_server/model/Store.dart';
import 'package:how_connect_to_server/screens/store/store.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

import '../my_basket/appbar.dart';
import '../my_basket/my_Basket.dart';
import '../my_basket/side_menu.dart';
import 'widgets/navigation.dart';
import '../profile/profile.dart';
import '../../model/clientBasket.dart';

class HomePage extends StatefulWidget {
  _HomePage createState() => new _HomePage();
}
//TODO store_step2 here we get fill the product from the dummy data.
// if we want to fill the product from firebase
// StoreData _storeData = getStoreFromFirebase();
StoreData storeData = getStoreDummyData();
ClientBasket clientBasket = getClientBasketDummyData();


class _HomePage extends State<HomePage> {

  int navigationIdx = 0;
  Widget currentScreen = new Store(storeData:storeData);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation)
      {
        SizeConfig().init(constraints, orientation);
        return Scaffold(
          key: _scaffoldKey,
          body: Stack(children: <Widget>[
            currentScreen,

            // new Store(),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(SizeConfig.getResponsiveWidth(40)),
                  topLeft: Radius.circular(SizeConfig.getResponsiveWidth(40)),
                ),
                child: BottomNavigationBar(
                  currentIndex: navigationIdx,
                  elevation: 40.0,
                  items: [
                    bottomNavigationBarItem("assets/images/sala.png",
                        "assets/images/salaActive.png", "Store"),
                    bottomNavigationBarItem("assets/images/sala.png",
                        "assets/images/salaActive.png", "My Cart"),
                    bottomNavigationBarItem("assets/images/heardw.png",
                        "assets/images/heartActive.png", "Favourite"),
                    bottomNavigationBarItem("assets/images/profile.png",
                        "assets/images/profileActive.png", "Profile")
                  ],
                  onTap: (index) {
                    navigationIdx = index;
                    currentScreen = getCurrentScreen(index);
                    setState(() {});
                  },
                  unselectedItemColor: Colors.black,
                  selectedItemColor: Colors.green,
                  showUnselectedLabels: true,
                ),
              ),
            )
          ]),
          appBar: myCartAppbar(
              _scaffoldKey, getTitleAppBarWidget(navigationIdx)),
          drawer: SideMenu(),
        );
      });
    });
  }
}

Widget getTitleAppBarWidget(int idx){
  if(idx == 1){
    return  Text("My Cart");
  }else {
    return Container();
  }
}

Widget getCurrentScreen(int idx) {
  if (idx == 0) {
    // TODO here we decide that we load dummy data .. if you want to load data from firebase just change getStoreDummyData.
    return Store(storeData: storeData,);
  } else if (idx == 1) {
    // TODO here we decide that we load dummy data .. if you want to load data from firebase just change getStoreDumm
    return new myBasket(clientBasket:clientBasket,);
  }else if(idx == 3) {
    return new Profile();
  } else{
    return new Store(storeData: storeData,);
  }
}
