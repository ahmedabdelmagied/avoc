import 'package:flutter/material.dart';

class aboutCompany extends StatefulWidget {
  _aboutCompany createState() => new _aboutCompany();
}


class _aboutCompany extends State<aboutCompany>{
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0XFF21d493),
          leading:Container(
            margin:EdgeInsets.only(top: 10.0),child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),),
          title:Container(margin:EdgeInsets.only(top: 10.0),child:Text("About Company",
              style: TextStyle(fontSize: 25.0, color: Colors.white))),
          elevation: 0.0,
        ),
        backgroundColor: Colors.white,
        body: ListView(

            children: <Widget>[

              Image.asset('assets/images/about_company.png'),
              Directionality(
                textDirection: TextDirection.ltr,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20,left: 15,right: 10,bottom: 20),
                  child: Text(
                    "aboutCompanyabout Companyabout CompanyaboutCompanya boutCompany"
                        "aboutCompanyaboutCompany aboutCompanyaboutCfompanyaboutCompany"
                        "aboutCompanyabout"
                        "CompanyaboutCompanyabout CompanyaboutCompanyaboutCompanyaboutCompany"
                        "aboutCompanyaboutCompanyaboutCompanyaboutCompanyaboutCompany",
                    style: TextStyle(
                      wordSpacing: 6,
                      fontSize: 20,
                      fontFamily: 'SourceSansPro',
                      color: Colors.black
                  ),
                  ),
                ),
              ),
            ],
          ),
        ),
    );
  }

}