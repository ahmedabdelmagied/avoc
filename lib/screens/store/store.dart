import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

import '../../model/Product.dart';
import '../../model/Store.dart';
import 'widgets/categoryTitle.dart';
import 'widgets/main_card.dart';
import 'widgets/product_card.dart';
import 'widgets/storeHeader.dart';
import 'package:device_simulator/device_simulator.dart';

class Store extends StatelessWidget {
  StoreData storeData;
  Store({this.storeData});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Container(
          child: Stack(children: <Widget>[
            new ListView(
              children: <Widget>[
                StoreHeader(),
                Container(
                  margin: EdgeInsets.only(
                      bottom: 0.781 * SizeConfig.heightMultiplier),
                  height: 18.55 * SizeConfig.heightMultiplier,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      new mainCard(
                        title: "Home",
                        imagePath: "assets/images/home.png",
                      ),
                      new mainCard(
                        title: "Vegatables",
                        imagePath: "assets/images/4.png",
                      ),
                      new mainCard(
                        title: "Fruits",
                        imagePath: "assets/images/5.png",
                      ),
                      new mainCard(
                        title: "Honey",
                        imagePath: "assets/images/7.png",
                      ),
                      new mainCard(
                        title: "Drink",
                        imagePath: "assets/images/9.png",
                      ),
                    ],
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(child: CategoryTitle(categoryName: "Vegatables")),
                  ],
                ),
                Container(
                    height: 24.4 * SizeConfig.heightMultiplier,
                    child: ListView.builder(scrollDirection: Axis.horizontal,
                        itemCount: storeData.getVegatables().length,
                        itemBuilder: (BuildContext context, int index) {
                         return productCard(
                           product: storeData.getVegatables().elementAt(index),
                         );
                      })
                
                ),
                CategoryTitle(categoryName: "Fruits"),
                Container(
                  height: 24.41 * SizeConfig.heightMultiplier,
                    child: ListView.builder(scrollDirection: Axis.horizontal,
                        itemCount: storeData.getVegatables().length,
                        itemBuilder: (BuildContext context, int index) {
                          return productCard(
                            product: storeData.getFruits().elementAt(index),
                          );
                        })
                ),
                CategoryTitle(categoryName: "Drinks"),
                Container(
                  margin: EdgeInsets.only(bottom: 90.0),
                  height: 24.14 * SizeConfig.heightMultiplier,
                    child: ListView.builder(scrollDirection: Axis.horizontal,
                        itemCount: storeData.getVegatables().length,
                        itemBuilder: (BuildContext context, int index) {
                          return productCard(
                            product: storeData.getDrinks().elementAt(index),
                          );
                        })
                ),
              ],
            ),
          ]),
        );
      });
    });
  }
}
