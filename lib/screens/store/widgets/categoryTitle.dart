import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class CategoryTitle extends StatelessWidget {
  String categoryName;

  CategoryTitle({this.categoryName});

  @override
  Widget build(BuildContext context) {


        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 2.7 * SizeConfig.widthMultiplier, bottom: 1.9*SizeConfig.heightMultiplier, top: 1.9 * SizeConfig.heightMultiplier ,),
              child: Text(
                categoryName,
                style: TextStyle(fontSize: 3.15 * SizeConfig.heightMultiplier),
              ),
            ),
            FlatButton(
                color: Colors.green,
                child: Container(
                  margin: EdgeInsets.all(SizeConfig.getResponsiveHeight(5.0)),
                  color: Colors.green,
                  child: Container(
                      margin: EdgeInsets.only(
                          top:SizeConfig.getResponsiveHeight(5) , right: SizeConfig.getResponsiveWidth(8.0), left: SizeConfig.getResponsiveWidth(8.0), bottom: SizeConfig.getResponsiveHeight(5.0)),
                      child: Text(
                        "See All",
                        style: TextStyle(color: Colors.white, fontSize: SizeConfig.getResponsiveHeight(12)),
                      )),
                ))
          ],
        );

  }
}
