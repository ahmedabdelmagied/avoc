import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class mainCard extends StatelessWidget {
  String imagePath;
  String title;

  mainCard({this.title, this.imagePath});

  @override
  Widget build(BuildContext context) {
    return FittedBox(
        fit: BoxFit.contain,
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    right: 0.0, top: 0.0, left: 1.0, bottom: 0.0),
                width: SizeConfig.getResponsiveHeight(25),
                height: SizeConfig.getResponsiveHeight(25),
                color: Colors.transparent,
                child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    child: Container(
                      child: Image.asset(imagePath),
                    ))),
            Container(
              margin: EdgeInsets.only(top: 0.0,left: 1.0),
              child: Text(title, style: TextStyle(fontSize: SizeConfig.getResponsiveHeight(4), )),
            )
          ],
        ));
  }
}
