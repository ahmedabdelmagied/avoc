import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

class StoreHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Container(
        child: Stack(
          children: <Widget>[

            Container(
              height: SizeConfig.getResponsiveHeight(94.0),
              decoration: BoxDecoration(
                  color: Color(0XFF21d493),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(SizeConfig.getResponsiveWidth(20.0)),
                      bottomRight: Radius.circular(SizeConfig.getResponsiveWidth(20.0)))),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Container(
                  margin: EdgeInsets.only(right: SizeConfig.getResponsiveWidth(5.0)),
                  child: Text(
                    "Delivery to",
                    style: TextStyle(fontSize: SizeConfig.getResponsiveWidth(15.0), color: Colors.white),
                  ),
                ),
                Container(
                  width: SizeConfig.getResponsiveWidth(230.0),
                  height: SizeConfig.getResponsiveWidth(30.0),
                  color: Colors.white70,
                  margin: EdgeInsets.only(bottom: SizeConfig.getResponsiveHeight(5.0)),
                  child: TextField(
                      style: TextStyle(
                        fontSize: SizeConfig.getResponsiveHeight(20.0),
                        color: Colors.blueAccent,
                      ),
                      decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: SizeConfig.getResponsiveWidth(12.0)),
                        prefixIcon: Icon(
                          Icons.place,
                          color: Color(0XFF21d493),
                        ),
                        prefixStyle: TextStyle(fontSize:SizeConfig.getResponsiveWidth(15.0) ),
                        hintText: "Egypt , Fayoum",
                      )),
                )
              ],
            ),
            Positioned(
              bottom: SizeConfig.getResponsiveHeight(8),
              left: SizeConfig.getResponsiveWidth(35.0),
              child: Container(
                width: SizeConfig.getResponsiveWidth(300.0),
                height: SizeConfig.getResponsiveHeight(30.0),
                color: Colors.white70,
                margin: EdgeInsets.only(bottom: SizeConfig.getResponsiveHeight(10.0)),
                child: TextField(
                    style: TextStyle(
                      fontSize: SizeConfig.getResponsiveWidth(20.0),
                      color: Colors.blueAccent,
                    ),
                    decoration: InputDecoration(
                      hintStyle: TextStyle(fontSize: SizeConfig.getResponsiveWidth(12.0)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0XFF21d493),
                      ),
                      hintText: "Search for Category",
                    )),
              ),
            )
          ],
        ));
  }
}
