






import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:how_connect_to_server/utils/size_config.dart';

import '../../../model/Product.dart';
import '../../product/product_details.dart';

class productCard extends StatefulWidget {
  Product product;
  int quantity = 5;

  productCard({this.product, this.quantity});

  @override
  _productCardState createState() => _productCardState();
}

class _productCardState extends State<productCard> {
  @override
  Widget build(BuildContext context) {
    return FittedBox(fit: BoxFit.contain,child:Container(

        margin: EdgeInsets.only(left:12.0 , right: 10.0),
        width: SizeConfig.getResponsiveWidth(370.0),
        height:SizeConfig.getResponsiveHeight(280.0) ,
        child:Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          color: Colors.white,
          elevation: 3,
          child: Column(

            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[      Container(
                  margin: EdgeInsets.only(top:SizeConfig.getResponsiveHeight(10.0) , left:SizeConfig.getResponsiveWidth(20.0)),
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius:
                      new BorderRadius.all(Radius.circular(5.0))),
                  width: SizeConfig.getResponsiveWidth(90.0),
                  height: SizeConfig.getResponsiveHeight(30.0),
                  child: Center(
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: Text(
                        "${widget.product.productQuantity.toString()} kg",
                        style: new TextStyle(
                            color: Colors.white70,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.getResponsiveWidth(25.0)),
                      ),
                    ),
                  ),
                ),
                  Container(
                    margin: EdgeInsets.all(15.0),
                    child: new Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size:SizeConfig.getResponsiveHeight(45.0),
                    ),
                  )
                ],
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (context) =>
                          new productDetails(widget.product, widget.quantity)));
                },
                child: FittedBox(
                  fit: BoxFit.cover,
                  child:Container(
                  height: SizeConfig.getResponsiveHeight(130.0),
                  width: SizeConfig.getResponsiveWidth(390.0),
                  child: Image.asset(widget.product.ImagePath ,width: SizeConfig.getResponsiveWidth(390.0),height: SizeConfig.getResponsiveHeight(150.0), fit: BoxFit.contain,alignment: Alignment.center,),
                ),)
              ),

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: SizeConfig.getResponsiveWidth(9.0)),
                    child: Text(
                      "${widget.product.productName}",
                      maxLines: 3,
                      //   overflow:TextOverflow. ,
                      style: TextStyle(
                        fontSize: SizeConfig.getResponsiveWidth(20.0),
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: SizeConfig.getResponsiveWidth(12.0)),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "LE ${widget.product.productPrice.toString()}",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontWeight:  FontWeight.bold,
                            fontSize: SizeConfig.getResponsiveWidth(15),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: SizeConfig.getResponsiveWidth(15.0), bottom: SizeConfig.getResponsiveHeight(10.0)),
                        child: Icon(
                          Icons.shopping_basket,
                          color: Colors.green,
                          size: SizeConfig.getResponsiveHeight(35),
                        ),
                      )
                    ],
                  )
                ],
              ),

            ],
          ),
        )));
  }
}
